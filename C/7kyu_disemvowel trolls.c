/**
 ******************************************************************************
 *                   COPYRIGHT 2022, Daniel "Thready" Nitecki
 ******************************************************************************
 * Author: Daniel "Thready" Nitecki
 */

#include <assert.h>
//solution must allocate all required memory
//and return a free-able buffer to the caller.

#include "string.h"
#include <stdlib.h>

char *disemvowel(const char *str) {
  char *ret = (char *)malloc(1024);
  memset(ret, 0, 1024);
  
  int i = 0;
  int j = 0;
  while (str[i] != '\0') {
    switch (str[i]) {
      case 'a':
      case 'e':
      case 'i':
      case 'o':
      case 'u':
      case 'A':
      case 'E':
      case 'I':
      case 'O':
      case 'U':
        break;

      default: {
        ret[j] = str[i];
        j++;
        break;
      }
    }
    i++;
  }
  return ret;
}

int main(void) {
  int result = strcmp(disemvowel("This website is for losers LOL!"), "Ths wbst s fr lsrs LL!");
  return 0;
}
