/**
 ******************************************************************************
 *                   COPYRIGHT 2022, Daniel "Thready" Nitecki
 ******************************************************************************
 * Author: Daniel "Thready" Nitecki
 */

#include <assert.h>
#include <limits.h>
#include <stdio.h>
#include <string.h>

void high_and_low(const char *strnum, char *result) {
  int lowest = INT_MAX;
  int highest = INT_MIN;

  char rest[1024 * 5] = {0};
  int actual = 0;
  int i = 0;
  while (strnum[i] != '\0') {
    if (strnum[i] == ' ') {
      rest[i] = '_';
    } else {
      rest[i] = strnum[i];
    }
    i++;
  }

  while (rest[0] != '\0') {
    int result = sscanf(rest, "%d_%s", &actual, rest);
    if (actual > highest) {
      highest = actual;
    }
    if (actual < lowest) {
      lowest = actual;
    }
    if (result != 2) {
      break;
    }
  }

  sprintf(result, "%d %d", highest, lowest);
}

int main(void) {
  char result[1024] = {0};

  high_and_low("8 3 -5 42 -1 0 0 -9 4 7 4 -4", result);
  assert(strcmp(result, "42 -9"));
  memset(result, 0, sizeof(result));

  high_and_low("1 2 3", result);
  assert(strcmp(result, "3 1"));
  memset(result, 0, sizeof(result));

  return 0;
}
