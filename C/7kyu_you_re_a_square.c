/**
 ******************************************************************************
 *                   COPYRIGHT 2022, Daniel "Thready" Nitecki
 ******************************************************************************
 * Author: Daniel "Thready" Nitecki
 */

#include <assert.h>
#include <math.h>
#include <stdbool.h>

bool is_square(int n) {
  bool ret = false;
  if (n >= 0) {
    ret = (0 == ((int)(sqrt(n) * 1000) % 1000));
  }
  return ret;
}

int main(void) {
  assert(is_square(-1) == false);
  assert(is_square(0) == true);
  assert(is_square(3) == false);
  assert(is_square(4) == true);
  assert(is_square(25) == true);
  assert(is_square(26) == false);
  return 0;
}
