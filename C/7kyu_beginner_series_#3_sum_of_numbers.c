/**
 ******************************************************************************
 *                   COPYRIGHT 2022, Daniel "Thready" Nitecki
 ******************************************************************************
 * Author: Daniel "Thready" Nitecki
 */

#include <assert.h>

int get_sum(int a, int b) {
  int ret = 0;
  if (a == b) {
    ret = a;
  } else {
    int i = a;
    int max = b;
    if (a > b) {
      i = b;
      max = a;
    }
    for (; i <= max; i++) {
      ret += i;
    }
  }
  return ret;
}

int main(void) {
  assert(get_sum(-1, -5) == -15);
  assert(get_sum(-5, -5) == -5);
  assert(get_sum(-50, 0) == -1275);
  assert(get_sum(321, 123) == 44178);
  assert(get_sum(505, 4) == 127759);
  assert(get_sum(-504, 4) == -127250);
  return 0;
}
