/**
 ******************************************************************************
 *                   COPYRIGHT 2022, Daniel "Thready" Nitecki
 ******************************************************************************
 * Author: Daniel "Thready" Nitecki
 */

#include <assert.h>
#include <stddef.h>

long sum_two_smallest_numbers(size_t n, const int* numbers) {
  unsigned long int lowest1 = (unsigned long int)-1;
  unsigned long int lowest2 = (unsigned long int)-1;
  for (int i = 0; i < n; i++) {
    if (lowest1 > numbers[i]) {
      if (lowest2 > numbers[i]) {
        lowest1 = lowest2;
        lowest2 = numbers[i];
      } else {
        lowest1 = numbers[i];
      }
    }
  }
  return lowest1 + lowest2;
}

int main(void) {
  const int numbers1[5] = {19, 5, 42, 2, 77};
  assert(sum_two_smallest_numbers(5, numbers1) == 7);
  const int numbers2[5] = {5, 8, 12, 19, 22};
  assert(sum_two_smallest_numbers(5, numbers2) == 13);
  return 0;
}
