/**
 ******************************************************************************
 *                   COPYRIGHT 2022, Daniel "Thready" Nitecki
 ******************************************************************************
 * Author: Daniel "Thready" Nitecki
 */

#include <assert.h>
#include <stddef.h>

size_t get_count(const char *s) {
  size_t ret = 0;
  while (*s != '\0') {
    switch (*s) {
      case 'a':
      case 'e':
      case 'i':
      case 'o':
      case 'u':
        ret++;
        break;

      default: {
        break;
      }
    }
    s++;
  }

  return ret;
}

int main(void) {
  assert(get_count("abracadabra") == 5);
  assert(get_count("") == 0);
  return 0;
}
