/**
 ******************************************************************************
 *                   COPYRIGHT 2022, Daniel "Thready" Nitecki
 ******************************************************************************
 * Author: Daniel "Thready" Nitecki
 */

#include <assert.h>
#include <stdio.h>
#include <string.h>

int largest_five_digits(const char *digits) {
  int ret = 0;
  char ret_str[6] = {0};

  int i = 0;
  while (digits[i + 4] != '\0') {
    strncpy(ret_str, &digits[i], sizeof(ret_str) - 1);
    int temp = 0;
    sscanf(ret_str, "%d", &temp);
    if (temp > ret) {
      ret = temp;
    }
    i++;
  }

  return ret;
}

int main(void) {
  assert(largest_five_digits("1234567890") == 67890);
  assert(largest_five_digits("283910") == 83910);
  assert(largest_five_digits("731674765") == 74765);
  return 0;
}
