/**
 ******************************************************************************
 *                   COPYRIGHT 2022, Daniel "Thready" Nitecki
 ******************************************************************************
 * Author: Daniel "Thready" Nitecki
 */

#include <assert.h>
#include <stdbool.h>
#include <stddef.h>

bool in_asc_order(const int *arr, size_t arr_size) {
  bool ret = false;
  if (arr_size == 1) {
    ret = true;
  } else {
    int last = 0;
    for (size_t i = 0; i < arr_size; i++) {
      if (arr[i] < last) {
        ret = false;
        break;
      } else {
        ret = true;
        last = arr[i];
      }
    }
  }
  return ret;
}

int main(void) {
  const int arr0[] = {1, 2, 4, 7, 19};
  const int arr1[] = {1, 2, 3, 4, 5};
  const int arr2[] = {1, 6, 10, 18, 2, 4, 20};
  const int arr3[] = {9, 8, 7, 6, 5, 4, 3, 2, 1};

  assert(in_asc_order(arr0, 5) == true);
  assert(in_asc_order(arr1, 5) == true);
  assert(in_asc_order(arr2, 7) == false);
  assert(in_asc_order(arr3, 9) == false);
  return 0;
}
