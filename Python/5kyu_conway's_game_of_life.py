import numpy as np

def trim_zeros(arr):
    slices = tuple(slice(idx.min(), idx.max() + 1) for idx in np.nonzero(arr))
    return arr[slices]

def sum_neighbours(w, k, cells):
    ret = 0
    if (0 <= w < len(cells)) and (0 <= k < len(cells[1])):
        for i in range(w - 1, w + 2):
            if 0 <= i < len(cells):
                for j in range(k - 1, k + 2):
                    if 0 <= j < len(cells[i]):
                        ret += cells[i][j]
        ret -= cells[w][k]
    return ret

def next_gen(cells):
    if cells:
        cells_local = np.array(cells)
        ret = np.array(cells)

        for w, enum_w in enumerate(ret):
            for k, enum_k in enumerate(ret[1]):
                if cells_local[w][k] == 1:
                    if 2 <= sum_neighbours(w, k, cells_local) <= 3:
                        ret[w][k] = 1
                    else:
                        ret[w][k] = 0
                elif sum_neighbours(w, k, cells_local) == 3:
                    ret[w][k] = 1

        if all(x == 0 for v in ret for x in v):
            ret = [[]]

        if not isinstance(ret, list):
            ret = ret.tolist()
    else:
        ret = []
    return ret
