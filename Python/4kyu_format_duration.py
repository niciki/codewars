import math


def format_duration_entry(value, unit, next_durations):
    ret = ""
    if value:
        ret += f'{value} ' + unit
        if value > 1:
            ret += "s"
        if next_durations:
            if not all(i == 0 for i in next_durations):
                count = 0
                for i in next_durations:
                    if i != 0:
                        count += 1
                if count == 1:
                    ret += " and "
                else:
                    ret += ", "
    return ret


def format_duration(seconds):
    ret = ""

    if seconds in (0, None, ''):
        ret = "now"
    else:
        seconds_left = seconds
        max_seconds = 60
        max_minutes = 60
        max_hours = 24
        max_days = 365

        years = math.floor(seconds_left / (max_days * max_hours * max_minutes * max_seconds))
        seconds_left -= years * max_days * max_hours * max_minutes * max_seconds
        days = math.floor((seconds_left) / (max_hours * max_minutes * max_seconds))
        seconds_left -= days * max_hours * max_minutes * max_seconds
        hours = math.floor((seconds_left) / (max_minutes * max_seconds))
        seconds_left -= hours * max_minutes * max_seconds
        minutes = math.floor((seconds_left) / (max_seconds))
        seconds_left -= minutes * max_seconds

        durations = [
            years,
            days,
            hours,
            minutes,
            seconds_left,
        ]
        ret += format_duration_entry(years, "year", durations[1:])
        ret += format_duration_entry(days, "day", durations[2:])
        ret += format_duration_entry(hours, "hour", durations[3:])
        ret += format_duration_entry(minutes, "minute", durations[4:])
        ret += format_duration_entry(seconds_left, "second", None)

    return ret