def filter_list(l):
    return [entry for entry in l if isinstance(entry, int)]
