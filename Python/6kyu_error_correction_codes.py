def even_parity(bits):
    ret = False
    for bit in bits:
        ret ^= bool(int(bit))
    return str(int(ret))


def correct(m, n, bits):
    ret = bits

    rows_parity = bits[m * n:m * (n + 1)]
    cols_parity = bits[m * (n + 1):m * (n + 1) + n]

    err_row = None
    for i in range(m):
        row = bits[i * n:(i + 1) * n]
        if even_parity(row) != rows_parity[i]:
            err_row = i
            break

    err_col = None
    for i in range(n):
        col = bits[i:m * n:n]
        if even_parity(col) != cols_parity[i]:
            err_col = i
            break

    err_idx = None
    if err_row is not None and err_col is not None:
        err_idx = n * err_row + err_col
    elif err_row is not None:
        err_idx = m * n + err_row
    elif err_col is not None:
        err_idx = m * (n + 1) + err_col

    if err_idx is not None:
        ret = bits[:err_idx] + str(int(bits[err_idx]) ^ 1) + bits[err_idx + 1:]

    return ret
