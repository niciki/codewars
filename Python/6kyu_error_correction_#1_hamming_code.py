def encode(string):
    ret = ''
    for char in string:
        bits = bin(ord(char)).replace('0b', '').zfill(8)
        for bit in bits:
            ret += 3 * bit
    return ret

def decode(string):
    ret = ''
    bits = ''
    while string:
        tri_bits, string = string[:3], string[3:]
        sum_tri_bits = 0
        for bit in tri_bits:
            sum_tri_bits += int(bit)
        if sum_tri_bits >= 2:
            bits += '1'
        else:
            bits += '0'
        if len(bits) == 8:
            int_char = 0
            for bit in bits:
                int_char = (int_char << 1) | int(bit)
            ret += chr(int_char)
            bits = ''
    return ret
