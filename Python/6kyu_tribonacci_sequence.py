def tribonacci(signature, n):
    ret = signature
    while len(ret) < n:
        ret += [sum(ret[-3::])]
    return ret[0:n]
