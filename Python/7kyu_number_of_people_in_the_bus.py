def number(bus_stops):
    return sum([bus_stop[0] - bus_stop[1] for bus_stop in bus_stops])
