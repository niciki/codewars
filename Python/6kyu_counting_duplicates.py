def duplicate_count(text):
    text = text.lower()
    count = 0
    while len(text):
        if text.count(text[0]) > 1:
            count += 1
        text = "".join(text.split(text[0]))
    return count
