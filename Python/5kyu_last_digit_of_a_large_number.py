cycle_dict = {
    "0": [0],
    "1": [1],
    "2": [2, 4, 8, 6],
    "3": [3, 9, 7, 1],
    "4": [4, 6],
    "5": [5],
    "6": [6],
    "7": [7, 9, 3, 1],
    "8": [8, 4, 2, 6],
    "9": [9, 1]
}


def last_digit(n1, n2):
    ret = 1
    if n2 > 0:
        cycle = cycle_dict[str(n1)[-1]]
        ret = cycle[(n2 - 1) % len(cycle)]
    return ret
