import itertools

lookup = {
    "1": ["1", "2", "4"],
    "2": ["1", "2", "3", "5"],
    "3": ["2", "3", "6"],
    "4": ["1", "4", "5", "7"],
    "5": ["2", "4", "5", "6", "8"],
    "6": ["3", "5", "6", "9"],
    "7": ["4", "7", "8"],
    "8": ["5", "7", "8", "9", "0"],
    "9": ["6", "8", "9"],
    "0": ["0", "8"]
}

def get_pins(observed):
    all_comb = []
    ret = []
    for c in observed:
        all_comb.append(lookup[c])

    for comb in itertools.product(*all_comb):
        ret.append(''.join(comb))
    return ret
