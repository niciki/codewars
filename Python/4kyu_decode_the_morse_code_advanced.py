def decode_bits(bits):
    bits = bits.strip('0')
    if bits == '1'*len(bits):
        t_unit = len(bits)
    else:
        t_unit = bits.find('0')
        if (t_unit % 3) == 0:
            t_unit_reduced = (int)(t_unit / 3)
            if((bits.find('0'+'1'*t_unit_reduced+'0') != -1) or (bits.find('1'+'0'*t_unit_reduced+'1') != -1)):
                t_unit = t_unit_reduced

    return bits.replace('1' * 3 * t_unit, '-').replace('1' * t_unit, '.').replace('0' * 7 * t_unit, '   ').replace('0' * 3 * t_unit, ' ').replace('0' * t_unit, '')


def decode_morse(morse_code):
    return ' '.join(''.join(MORSE_CODE[letter] for letter in word.split()) for word in morse_code.strip().split('   '))
