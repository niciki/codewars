import re


def decodeBitsAdvanced(bits):
    ret = ""

    bits = bits.strip('0')
    findings = re.findall(r'(1+|0+)', bits)

    # message contains only '1's
    if len(findings) == 1:
        ret = "."
    # non-trivial message
    elif len(findings) > 1:
        # minimum length of dot/new character
        unit_min = min(len(seq) for seq in findings)

        # maximum length of dash
        dash_max = max(len(seq) for seq in findings if seq[0] == '1')
        if dash_max < 3 * unit_min:
            dash_max = 3 * unit_min

        # maximum length of space
        space_max = max(len(seq) for seq in findings if seq[0] == '0')
        if space_max < 7 * unit_min:
            space_max = 7 * unit_min

        # thresholds
        dash_min = (unit_min + dash_max) / 2
        new_char_min = dash_min
        space_min = (new_char_min + space_max) / 2

        for seq in findings:
            if '1' in seq:
                if len(seq) >= dash_min:
                    ret += '-'
                else:
                    ret += '.'
            else:
                if len(seq) >= space_min:
                    ret += '   '
                elif len(seq) >= new_char_min:
                    ret += ' '
    return ret


def decodeMorse(morse_code):
    ret = ""
    for word in morse_code.strip().split('   '):
        for letter in word.split():
            ret += MORSE_CODE[letter]
        ret += " "
    return ret[:-1]
