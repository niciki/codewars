def printer_error(s):
    allowed_chars = 'abcdefghijklm'

    num = 0
    den = 0

    for c in s:
        den = den + 1
        if c not in allowed_chars:
            num = num + 1

    return f'{num}/{den}'
