def in_array(array1, array2):
    return sorted(list(set([x for x in array1 for y in array2 if x in y])))
