def unique_in_order(iterable):
    ret = []
    if len(iterable):
        ret = [iterable[0]]
        last = iterable[0]
        for x in iterable:
            if x != last:
                ret.append(x)
            last = x
    return ret
