*You don't need python knowledge to complete this kata*

You've waited weeks for this moment. Finally an NSA agent has opened the malware you planted on their system.
Now you can execute code remotly.
It looks like you're on a Linux machine. Your goal is to root the box and return the content of root.txt

You don't have a complete remote shell, you can only run a few commands.
You got access on multiple machines, the boxNr shows you, on which machine you are currently.

Return the command you want to execute:
```python
# example
return "help"
```
It prints for example
```
available commands: whoami, pwd, cd, ls, help
```

Another example:

```python
# example2
return "whoami"
```
It prints for example
```
root
```
Some hints:
1. If you want to execute more commands in a row, it works exactly like in bash.
2. Pipes aren't working
3. The available commands aren't always the same
4. Just because the name of a command is the same here as in bash, it doesn't mean that it works the same
5. Your goal is to hack the NSA, therefore you are allowed to run things on your local machine. You don't have to run everything on codewars.

