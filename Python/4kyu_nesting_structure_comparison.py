def describe_struct(in_struct, in_ret):
    ret = in_ret
    if isinstance(in_struct, list):
        ret += f'{len(in_struct)}'
        for entry in in_struct:
            ret = describe_struct(entry, ret)
    else:
        ret += 'i'
    return ret


def same_structure_as(original, other):
    desc_original = ""
    desc_original = describe_struct(original, desc_original)

    desc_other = ""
    desc_other = describe_struct(other, desc_other)
    return desc_original == desc_other
