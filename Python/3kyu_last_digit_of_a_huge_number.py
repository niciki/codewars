cycle_dict = {
    "0": [0],
    "1": [1],
    "2": [2, 4, 8, 6],
    "3": [3, 9, 7, 1],
    "4": [4, 6],
    "5": [5],
    "6": [6],
    "7": [7, 9, 3, 1],
    "8": [8, 4, 2, 6],
    "9": [9, 1]
}


def last_digit_of_pow_of_two_integers(n1, n2):
    ret = 1
    if n2 > 0:
        cycle = cycle_dict[str(n1)[-1]]
        ret = cycle[(n2 - 1) % len(cycle)]
    return ret


def last_digit(lst):
    ret = 1
    if len(lst) == 1:
        ret = lst[0] % 10
    else:
        while len(lst) > 1:
            n2 = lst.pop()
            n1 = lst.pop()
            ret = last_digit_of_pow_of_two_integers(n1, n2)
            if n2 > 1:
                if (n1 % 10) <= 1:
                    lst.append(n1 ** 2)
                else:
                    lst.append((n1 % 100) ** ((n2 % 10) + 2))
            else:
                lst.append(ret)

    return ret
