import re


def to_jaden_case(string):
    return re.sub(r'( \w)', lambda pat: pat.group(1).upper(), string.capitalize())
