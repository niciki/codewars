from itertools import permutations as perm


def permutations(string):
    return [''.join(x) for x in set(perm(string))]
