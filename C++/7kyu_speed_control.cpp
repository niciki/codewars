/**
 ******************************************************************************
 *                   COPYRIGHT 2022, Daniel "Thready" Nitecki
 ******************************************************************************
 * Author: Daniel "Thready" Nitecki
 */

#include <assert.h>
#include <vector>
#include <math.h>

class GpsSpeed
{
public:
    static int gps(int s, std::vector<double> &x);
};

int GpsSpeed::gps(int s, std::vector<double> &x) {
    int max_speed = 0;
    if (x.back() - x.front() > 1) {
        for (int i = 0; i < x.size() - 1; i++) {
            int speed = 0;
            double diff = x[i + 1] - x[i];
            speed = floor((3600 * diff) / s);
            if (speed > max_speed) {
                max_speed = speed;
            }
        }
    }
    return max_speed;
}

int main(void) {
    std::vector<double> x = {0.0, 0.23, 0.46, 0.69, 0.92, 1.15, 1.38, 1.61};
    assert(GpsSpeed::gps(20, x) == 41);
    x = {0.0, 0.11, 0.22, 0.33, 0.44, 0.65, 1.08, 1.26, 1.68, 1.89, 2.1, 2.31, 2.52, 3.25};
    assert(GpsSpeed::gps(12, x) == 219);
    return 0;
}
