/**
 ******************************************************************************
 *                   COPYRIGHT 2022, Daniel "Thready" Nitecki
 ******************************************************************************
 * Author: Daniel "Thready" Nitecki
 */

#include <assert.h>

long long rowSumOddNumbers(unsigned n) {
  long first = n * (n - 1) + 1;
  long long ret = first;
  for (unsigned i = 1; i < n; i++) {
    ret += first + 2 * i;
  }
  return ret;
}

int main(void) {
  assert(rowSumOddNumbers(1) == 1);
  assert(rowSumOddNumbers(42) == 74088);
  return 0;
}
