/**
 ******************************************************************************
 *                   COPYRIGHT 2022, Daniel "Thready" Nitecki
 ******************************************************************************
 * Author: Daniel "Thready" Nitecki
 */

#include <assert.h>
#include <stdint.h>

#include <iostream>
#include <map>
#include <string>

using namespace std;

const std::map<char, int> mapping_roman = {
    {'I', 1},
    {'V', 5},
    {'X', 10},
    {'L', 50},
    {'C', 100},
    {'D', 500},
    {'M', 1000},
};

int solution(string roman) {
  int ret = 0;
  if (roman.size() == 1) {
    ret = mapping_roman.at(roman[0]);
  } else {
    for (uint16_t i = 0; i < roman.size() - 1; i++) {
      if (mapping_roman.at(roman[i + 1]) > mapping_roman.at(roman[i])) {
        ret += mapping_roman.at(roman[i + 1]) - mapping_roman.at(roman[i]);
        i++;
      } else {
        ret += mapping_roman.at(roman[i]);
      }
      if (i == roman.size() - 2) {
        ret += mapping_roman.at(roman[i + 1]);
      }
    }
  }
  return ret;
}

int main(void) {
  assert(solution("XCV") == 95);
  assert(solution("MCMXC") == 1990);
  assert(solution("I") == 1);
  assert(solution("IV") == 4);
  assert(solution("MMVIII") == 2008);
  assert(solution("MDCLXVI") == 1666);
  return 0;
}
