/**
 ******************************************************************************
 *                   COPYRIGHT 2022, Daniel "Thready" Nitecki
 ******************************************************************************
 * Author: Daniel "Thready" Nitecki
 */

#include <assert.h>
#include <stdint.h>

#include <string>
#include <algorithm>

typedef enum {
  kAsciiToIntOffset = 0x30,
} GeneralEnums;

static const uint8_t thirt_divisor[] = {
    1,
    10,
    9,
    12,
    3,
    4};

class Thirteen {
 public:
  static long long thirt(long long n);
};

long long Thirteen::thirt(long long n) {
  std::string str_n = std::to_string(n);
  uint16_t idx = 0;
  long long ret = 0;
  for (int i = str_n.size() - 1; i >= 0; i--) {
    uint8_t multiplier = thirt_divisor[idx % sizeof(thirt_divisor)];
    ret += (str_n[i] - kAsciiToIntOffset) * multiplier;
    idx++;
  }
  if (ret != n) {
    ret = Thirteen::thirt(ret);
  }
  return ret;
}

int main(void) {
  assert(Thirteen::thirt(8529) == 79);
  assert(Thirteen::thirt(85299258) == 31);
  assert(Thirteen::thirt(5634) == 57);
  assert(Thirteen::thirt(1111111111) == 71);
  assert(Thirteen::thirt(987654321) == 30);
  return 0;
}
