/**
 ******************************************************************************
 *                   COPYRIGHT 2022, Daniel "Thready" Nitecki
 ******************************************************************************
 * Author: Daniel "Thready" Nitecki
 */

#include <assert.h>
#include <vector>
#include <string>

class MaxDiffLength
{
public:
    static int mxdiflg(std::vector<std::string> &a1, std::vector<std::string> &a2);
};

static std::pair<unsigned int, unsigned int> get_min_max_length(std::vector<std::string> &a) {
    unsigned int a_min = (unsigned int)(-1);
    unsigned int a_max = 0;
    for (auto &&i : a) {
        if (a_min > i.size()) {
            a_min = i.size();
        }
        if (a_max < i.size()) {
            a_max = i.size();
        }
    }
    return {a_min, a_max};
}

int MaxDiffLength::mxdiflg(std::vector<std::string> &a1, std::vector<std::string> &a2) {
    int ret = -1;
    if (a1.size() && a2.size()) {
        std::pair<unsigned int, unsigned int> a1_min_max = get_min_max_length(a1);
        std::pair<unsigned int, unsigned int> a2_min_max = get_min_max_length(a2);
        int dist1 = abs((int)(a1_min_max.first - a2_min_max.second));
        int dist2 = abs((int)(a1_min_max.second - a2_min_max.first));
        if (dist1 > dist2) {
            ret = dist1;
        } else {
            ret = dist2;
        }
    }
    return ret;
}

int main(void) {
    std::vector<std::string> s1 = {"hoqq", "bbllkw", "oox", "ejjuyyy", "plmiis", "xxxzgpsssa", "xxwwkktt", "znnnnfqknaz", "qqquuhii", "dvvvwz"};
    std::vector<std::string> s2 = {"cccooommaaqqoxii", "gggqaffhhh", "tttoowwwmmww"};
    assert(MaxDiffLength::mxdiflg(s1, s2) == 13);
    s1 = {"ejjjjmmtthh", "zxxuueeg", "aanlljrrrxx", "dqqqaaabbb", "oocccffuucccjjjkkkjyyyeehh"};
    s2 = {"bbbaaayddqbbrrrv"};
    assert(MaxDiffLength::mxdiflg(s1, s2) == 10);
    return 0;
}
