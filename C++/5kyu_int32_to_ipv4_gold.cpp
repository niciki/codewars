/**
 ******************************************************************************
 *                   COPYRIGHT 2022, Daniel "Thready" Nitecki
 ******************************************************************************
 * Author: Daniel "Thready" Nitecki
 */

#include <assert.h>
#include <string>

std::string uint32_to_ip(uint32_t ip) {
  std::string ret = "";
  ret = std::to_string(ip >> 24) + '.'
    + std::to_string(ip >> 16 & 0xFF) + '.'
    + std::to_string(ip >> 8 & 0xFF) + '.'
    + std::to_string(ip & 0xFF);
  return ret;
}

int main(void) {
  assert(uint32_to_ip(2154959208) == "128.114.17.104");
  assert(uint32_to_ip(0) == "0.0.0.0");
  assert(uint32_to_ip(2149583361) == "128.32.10.1");
  return 0;
}
