#include <string>

using namespace std;

#include <iostream>

string sliceString (string str )
{
  str.erase(str.begin());
  str.erase(str.end() - 1);
  return str;
}


int main(void) {
  std::cout << sliceString("abcd") << std::endl;
  return 0;
}