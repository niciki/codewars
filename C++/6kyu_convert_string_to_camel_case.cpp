/**
 ******************************************************************************
 *                   COPYRIGHT 2022, Daniel "Thready" Nitecki
 ******************************************************************************
 * Author: Daniel "Thready" Nitecki
 */

#include <assert.h>
#include <string>

std::string to_camel_case(std::string text) {
  std::string ret = "";
  bool is_to_upper = false;
  for (auto &&c : text) {
    if (c == '_' || c == '-') {
      is_to_upper = true;
    } else {
      if (is_to_upper) {
        ret += toupper(c);
        is_to_upper = false;
      } else {
        ret += c;
      }
    }
  }
  return ret;
}

int main(void) {
    assert(to_camel_case("") == "");
    assert(to_camel_case("the_stealth_warrior") == "theStealthWarrior");
    assert(to_camel_case("The-Stealth-Warrior") == "TheStealthWarrior");
    assert(to_camel_case("A-B-C") == "ABC");
    return 0;
}
