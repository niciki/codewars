/**
 ******************************************************************************
 *                   COPYRIGHT 2022, Daniel "Thready" Nitecki
 ******************************************************************************
 * Author: Daniel "Thready" Nitecki
 */

#include <assert.h>

// ---------------------
// gold
// ---------------------
#define 🙌 (
#define 🎁 return
#define 🤞 )
#define 😢 -
#define 😁 *
#define 🎉 ;
#define 👀 int
#define 🤲 2
#define 🤚 180
#define 🤷‍ {
#define 🤦‍ }
#define 🤔 angle

👀 🤔 🙌 👀 🎶 🤞 🤷‍ 🎁 🙌 🎶 😢 🤲 🤞 😁 🤚 🎉 🤦‍

// ---------------------

int angle(int n) {
  return (n - 2) * 180;
}

int main(void) {
    assert(angle(3) == 180);
    assert(angle(4) == 360);
    return 0;
}
