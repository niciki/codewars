#include <iostream>
#include <string>
#include <vector>

std::string switch_it_up_gold(int number) {
  return std::vector<std::string>{"Zero", "One", "Two", "Three", "Four", "Five", "Six", "Seven", "Eight", "Nine"}[number];
}

std::string switch_it_up(int number) {
  std::string ret = "";
  switch (number) {
    case 0:
      ret = "Zero";
      break;

    case 1:
      ret = "One";
      break;

    case 2:
      ret = "Two";
      break;

    case 3:
      ret = "Three";
      break;

    case 4:
      ret = "Four";
      break;

    case 5:
      ret = "Five";
      break;

    case 6:
      ret = "Six";
      break;

    case 7:
      ret = "Seven";
      break;

    case 8:
      ret = "Eight";
      break;

    case 9:
      ret = "Nine";
      break;

    default:
      break;
  }
  return ret;
}

int main(void) {
  std::cout << switch_it_up(0) << std::endl;
  std::cout << switch_it_up(1) << std::endl;
  std::cout << switch_it_up(2) << std::endl;
  std::cout << switch_it_up(3) << std::endl;
  std::cout << switch_it_up(4) << std::endl;
  std::cout << switch_it_up(5) << std::endl;
  std::cout << switch_it_up(6) << std::endl;
  std::cout << switch_it_up(7) << std::endl;
  std::cout << switch_it_up(8) << std::endl;
  std::cout << switch_it_up(9) << std::endl;
  return 0;
}
