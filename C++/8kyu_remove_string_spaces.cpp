#include <string>
#include <iostream>
#include <algorithm>

std::string no_space(std::string x) {
  x.erase(std::remove(x.begin(), x.end(), ' '), x.end());
  return x;
}

int main(void) {
  std::cout << "Hello" << std::endl;
  std::cout << no_space("8 j 8   mBliB8g  imjB8B8  jl  B") << std::endl;
  return 0;
}