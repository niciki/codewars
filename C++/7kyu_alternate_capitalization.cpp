/**
 ******************************************************************************
 *                   COPYRIGHT 2022, Daniel "Thready" Nitecki
 ******************************************************************************
 * Author: Daniel "Thready" Nitecki
 */

#include <assert.h>

#include <string>
#include <utility>
#include <cctype>

std::pair<std::string, std::string> capitalize(const std::string &s) {
    std::string ret1 = "";
    std::string ret2 = "";

    if (s.length()) {
        bool toggle = true;
        for (auto &&i : s) {
            if (toggle) {
                ret1 += toupper(i);
                ret2 += i;
            } else {
                ret1 += i;
                ret2 += toupper(i);
            }
            toggle = !toggle;
        }
    }
    return {ret1, ret2};
}

int main(void) {
    assert(capitalize("abcdef").first == "AbCdEf");
    assert(capitalize("abcdef").second == "aBcDeF");
    assert(capitalize("codewars").first == "CoDeWaRs");
    assert(capitalize("codewars").second == "cOdEwArS");
    assert(capitalize("abracadabra").first == "AbRaCaDaBrA");
    assert(capitalize("abracadabra").second == "aBrAcAdAbRa");
    assert(capitalize("codewarriors").first == "CoDeWaRrIoRs");
    assert(capitalize("codewarriors").second == "cOdEwArRiOrS");
    return 0;
}
