#include <string>
#include <map>

const std::map<char, char> mapping_gold = {
  {'A', 'T'},
  {'T', 'A'},
  {'C', 'G'},
  {'G', 'C'},
};

std::string DNAStrand_gold(const std::string& dna) {
  std::string ret = "";

  for (auto &&c : dna) {
    ret += mapping_gold.at(c);
  }
  return ret;
}

// ------------------
#include <assert.h>

std::string DNAStrand(const std::string& dna) {
  std::string ret = "";
  if (dna.length()) {
    for (auto &&i : dna) {
      if (i == 'A') {
        ret += 'T';
      } else if (i == 'T') {
        ret += 'A';
      } else if (i == 'C') {
        ret += 'G';
      } else if (i == 'G') {
        ret += 'C';
      }
    }
  }
  return ret;
}

int main(void) {
  assert(DNAStrand_gold("AAAA") == "TTTT");
  assert(DNAStrand_gold("ATTGC") == "TAACG");
  assert(DNAStrand_gold("GTAT") == "CATA");
  return 0;
}
