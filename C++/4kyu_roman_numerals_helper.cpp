/**
 ******************************************************************************
 *                   COPYRIGHT 2022, Daniel "Thready" Nitecki
 ******************************************************************************
 * Author: Daniel "Thready" Nitecki
 */

#include <assert.h>
#include <string>
#include <map>

const std::map<char, int> mapping_roman = {
  {'I', 1},
  {'V', 5},
  {'X', 10},
  {'L', 50},
  {'C', 100},
  {'D', 500},
  {'M', 1000},
};

const std::map<unsigned int, std::string> mapping_decimal = {
  {0, ""},
  {1, "I"},
  {2, "II"},
  {3, "III"},
  {4, "IV"},
  {5, "V"},
  {6, "VI"},
  {7, "VII"},
  {8, "VIII"},
  {9, "IX"},
  {10, "X"},
  {20, "XX"},
  {30, "XXX"},
  {40, "XL"},
  {50, "L"},
  {60, "LX"},
  {70, "LXX"},
  {80, "LXXX"},
  {90, "XC"},
  {100, "C"},
  {200, "CC"},
  {300, "CCC"},
  {400, "CD"},
  {500, "D"},
  {600, "DC"},
  {700, "DCC"},
  {800, "DCCC"},
  {900, "CM"},
  {1000, "M"},
  {2000, "MM"},
  {3000, "MMM"},
};

class RomanHelper {
 public:
    std::string to_roman(unsigned int n) {
      std::string ret = "";
      unsigned int decimal_digit = 1;
      while (n > 0) {
        unsigned int decimal = (n % 10) * decimal_digit;
        ret.insert(0, mapping_decimal.at(decimal));
        decimal_digit *= 10;
        n /= 10;
      }
      return ret;
    }

    int from_roman(std::string rn) {
      int ret = 0;
      if (rn.size() == 1) {
        ret = mapping_roman.at(rn[0]);
      } else {
        for (uint16_t i = 0; i < rn.size() - 1; i++) {
          if (mapping_roman.at(rn[i + 1]) > mapping_roman.at(rn[i])) {
            ret += mapping_roman.at(rn[i + 1]) - mapping_roman.at(rn[i]);
            i++;
          } else {
            ret += mapping_roman.at(rn[i]);
          }
          if (i == rn.size() - 2) {
            ret += mapping_roman.at(rn[i + 1]);
          }
        }
      }
      return ret;
    }
} RomanNumerals;

int main(void) {
  assert(RomanNumerals.from_roman("XXI") == 21);
  assert(RomanNumerals.from_roman("I") == 1);
  assert(RomanNumerals.from_roman("IX") == 9);
  assert(RomanNumerals.from_roman("IV") == 4);
  assert(RomanNumerals.from_roman("MMVIII") == 2008);
  assert(RomanNumerals.from_roman("MDCLXVI") == 1666);

  assert(RomanNumerals.to_roman(1000) == "M");
  assert(RomanNumerals.to_roman(4) == "IV");
  assert(RomanNumerals.to_roman(1) == "I");
  assert(RomanNumerals.to_roman(9) == "IX");
  assert(RomanNumerals.to_roman(1990) == "MCMXC");
  assert(RomanNumerals.to_roman(2008) == "MMVIII");
  assert(RomanNumerals.to_roman(3999) == "MMMCMXCIX");
  return 0;
}
