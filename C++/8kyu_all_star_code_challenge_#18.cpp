#include <string>
#include <assert.h>

unsigned strCount_gold(const std::string& word, char letter) {
  return std::count(word.cbegin(), word.cend(), letter);
}

unsigned int strCount(std::string word, char letter) {
  unsigned int ret = 0;

  if (word.length()) {
    for (auto &&i : word) {
      if (i == letter) {
        ret++;
      }
    }
  }
  return ret;
}

int main(void) {
  assert(strCount("", 'l') == 0);
  assert(strCount("Hello", 'o') == 1);
  assert(strCount("Hello", 'l') == 2);
  assert(strCount("Hello", 'q') == 0);
  assert(strCount("Pippi", 'p') == 2);
  return 0;
}
