#include <string>
#include <iostream>

std::string fakeBin(std::string str) {
  std::string ret = "";
  for (auto ch : str) {
    if (ch < '5') {
      ret += "0";
    } else {
      ret += "1";
    }
  }
  std::cout << std::endl;
  return ret;
}

int main(void) {
  std::cout << fakeBin("12345678901234567890") << std::endl;
  return 0;
}