/**
 ******************************************************************************
 *                   COPYRIGHT 2022, Daniel "Thready" Nitecki
 ******************************************************************************
 * Author: Daniel "Thready" Nitecki
 */

#include <assert.h>
#include <string>

std::string uint32_to_ip(uint32_t ip) {
  std::string ret = "";
  int octets = 4;
  while (ip > 0) {
    octets--;
    if (ret.length() > 0) {
      ret = "." + ret;
    }
    uint8_t octet = ip % 256;
    ip = ip / 256;
    ret = std::to_string(octet) + ret;
  }
  while(octets > 0) {
    octets--;
    if (ret.length() > 0) {
      ret = "." + ret;
    }
    ret = "0" + ret;
  }
  return ret;
}

int main(void) {
  assert(uint32_to_ip(2154959208) == "128.114.17.104");
  assert(uint32_to_ip(0) == "0.0.0.0");
  assert(uint32_to_ip(2149583361) == "128.32.10.1");
  return 0;
}
